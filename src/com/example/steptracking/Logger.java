package com.example.steptracking;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import android.annotation.TargetApi;
import android.net.wifi.ScanResult;
import android.os.Build;
import android.os.SystemClock;

public class Logger {
	
	private static Logger theOne = null;
	private BufferedWriter writer;
	private volatile boolean active = true;
	
	public static Logger getInstance() {
		if (theOne == null) {
			theOne = new Logger();
		}
		return theOne;
	}
	
	private Logger(){
		File file = new File("/storage/emulated/legacy/steptrace " + filenameTimestamp() + ".txt");
		FileWriter fw;
		try {
			fw = new FileWriter(file.getAbsoluteFile());
			writer = new BufferedWriter(fw);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
	
	public String filenameTimestamp(){
		SimpleDateFormat s = new SimpleDateFormat("yyyyMMddHHmmss", Locale.US);
		String format = s.format(new Date());
		return format;
	}
	
	public String timestamp(){
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
		String format = s.format(new Date());
		return format;
	}
	
	public synchronized void logWifi(List<String> wifiResults) {
		if (!active) return;
		
		try {
			writer.write(timestamp()+"\t");
			writer.write("WIFI");
			writer.newLine();
			for(String line : wifiResults) {
				writer.write(line);
				writer.newLine();
			}
			writer.write("----------ENDWIFI");
			writer.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	@TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
	private long getWifiEpochTimestamp(ScanResult scanResult) {
		return System.currentTimeMillis() - SystemClock.elapsedRealtime() + (scanResult.timestamp / 1000);
	}
	
	private String convertEpochTimestampToString(long timestamp) {
		SimpleDateFormat s = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS", Locale.US);
		String format = s.format(new Date(timestamp));
		return format;
	}
	
	public synchronized void logCompass(float[] orientVals) {
		if (!active) return;
		
		try {
			writer.write(timestamp()+"\t");
			writer.write("COMPASS\t");
			for(float val : orientVals) {
				writer.write(val+"\t");
			}
			writer.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void logStep() {
		if (!active) return;
		
		try {
			writer.write(timestamp()+"\t");
			writer.write("STEP\t");
			writer.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void logAccelerometer(float[] accelVals) {
		if (!active) return;
		
		try {
			writer.write(timestamp()+"\t");
			writer.write("ACCELEROMETER\t");
			for(float val : accelVals) {
				writer.write(val+"\t");
			}
			writer.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void logPathPosition(int id) {
		if (!active) return;
		
		try {
			writer.write(timestamp()+"\t");
			writer.write("POSITIONID\t");
			writer.write(id+"\t");
			writer.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void logMessage(String message) {
		if (!active) return;
		
		try {
			writer.write(timestamp()+"\t");
			writer.write("MESSAGE\t");
			writer.write(message+"\t");
			writer.newLine();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public synchronized void stopLogging(){
		try {
			active = false;
			writer.flush();
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
