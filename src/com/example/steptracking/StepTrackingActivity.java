package com.example.steptracking;

import java.util.List;

import com.example.steptracking.R;
import com.example.steptracking.TrackingService.MyBinder;

import eu.chainfire.libsuperuser.Shell;
import eu.chainfire.libsuperuser.Shell.Interactive;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class StepTrackingActivity extends Activity {
   
	private TextView mStepCountView;
	private TextView mWalkingStateView;
	private TextView mHeadingView;
	private TextView mWifiView;
	private EditText mIPtext;
	
	private Intent mIntent = null;
	
	private TrackingService mTrackingService = null;
	
	private int wifiScanCount = 0;
	private TextView mWifiScanCountView;
	private TextView mNextPositionIdEditView;
	private Interactive rootSession;
	private Button mStopLoggingButton;
	private CheckBox mActiveScanCheckBox;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        try{
        
	        setContentView(R.layout.activity_step_tracking);
	 
	        mStepCountView = (TextView)findViewById(R.id.StepCountTextView);
	        mWalkingStateView = (TextView)findViewById(R.id.WalkingStateTextView);
	        mHeadingView = (TextView)findViewById(R.id.HeadingTextView);
	        mIPtext = (EditText)findViewById(R.id.IPtext);
	        mWifiView = (TextView)findViewById(R.id.wifiTextView);
	        mWifiScanCountView = (TextView)findViewById(R.id.WifiScanCountTextView);
	        mNextPositionIdEditView = (TextView)findViewById(R.id.nextPositionEditText);
	        mStopLoggingButton = (Button)findViewById(R.id.stopLoggingButton);
	        mActiveScanCheckBox = (CheckBox)findViewById(R.id.activeScanCheckBox);
        }catch(Exception e)
        {
        	e.printStackTrace();
        }
        
    }
    
    
    @Override
    public void onStop() {
    	super.onStop();
    }
    
    @Override
    public void onDestroy() {
    	super.onDestroy();
    	
    	if(mTrackingService!=null)
    	{
    		mTrackingService.onDestroy();
    		this.getApplicationContext().unbindService(conn);
    	}
    	
    	mTrackingService = null;
    }
	
    @Override
    protected void onPause() {
        super.onPause();
    }
    
    @Override
    public void onResume() {
    	super.onResume();
    }
        
    public void onResetStepButtonClicked(View v) throws InterruptedException
    {
    	if(mTrackingService == null)
    	{
	    	mIntent = new Intent(StepTrackingActivity.this, TrackingService.class);
	    	this.getApplicationContext().bindService(mIntent, conn, Context.BIND_AUTO_CREATE);
    	}else
    		mTrackingService.onResetStepButtonClicked();

    }
    
    /*
    public class scanWifiInBackground implements Runnable {

		@Override
		public void run() {
			// TODO Auto-generated method stub
			boolean suAvailable = Shell.SU.available();
            if (suAvailable) {
                final String suVersion = Shell.SU.version(false);
                final String suVersionInternal = Shell.SU.version(true);
                
                runOnUiThread(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						mWifiScanCountView.setText("Root Obtained: "+suVersion+ " " + suVersionInternal);
					}
					
				});
            }else{
            	return;
            }
			
			while(true) {
			//final String output = shell.runCommandInShell("pwd");
				final List<String> output = Shell.SU.run("iw wlan0 scan freq 2412 2437 2462");
				Logger.getInstance().logWifi(output);
				wifiScanCount++;
				runOnUiThread(new Runnable(){

					@Override
					public void run() {
						// TODO Auto-generated method stub
						String outputLine="";
						if (output.size() > 1) {
							outputLine = output.get(0);
						}
						mWifiScanCountView.setText(wifiScanCount+"\n"+outputLine);
					}
					
				});
			}
		}
    	
    }*/
    
    private void sendRootCommand() {
    	String scanCommand = "iw wlan0 scan";
    	if (mActiveScanCheckBox.isChecked()) {
    		scanCommand = "iw wlan0 scan freq 2412 2437 2462";
    	}
    	
        rootSession.addCommand(new String[] { scanCommand }, 0,
                new Shell.OnCommandResultListener() {
            public void onCommandResult(int commandCode, int exitCode, final List<String> output) {
            	
                if (exitCode < 0) {
                    reportError("Error executing commands: exitCode " + exitCode);
                } else {
                	Logger.getInstance().logWifi(output);
    				wifiScanCount++;
    				runOnUiThread(new Runnable(){

    					@Override
    					public void run() {
    						String outputLine="";
    						if (output.size() > 1) {
    							outputLine = output.get(0);
    						}
    						mWifiScanCountView.setText(wifiScanCount+"\n"+outputLine);
    					}
    					
    				});
    				sendRootCommand();
                }
            }
        });
    }
    
    public void onScanWifiButtonClicked(View v) throws InterruptedException
    {
    	Toast.makeText(getApplicationContext(), "Wifi scan started", Toast.LENGTH_SHORT).show();
    	
    	rootSession = new Shell.Builder().
            useSU().
            setWantSTDERR(true).
            setWatchdogTimeout(5).
            setMinimalLogging(true).
            open(new Shell.OnCommandResultListener() {

                // Callback to report whether the shell was successfully started up 
                @Override
                public void onCommandResult(int commandCode, int exitCode, List<String> output) {
                    // note: this will FC if you rotate the phone while the dialog is up

                    if (exitCode != Shell.OnCommandResultListener.SHELL_RUNNING) {                    	
                    	reportError("Error opening root shell: exitCode " + exitCode);                    	
                    } else {
                        // Shell is up: send our first request 
                        sendRootCommand();
                    }
                }
            });
    	
    }
    
    protected void reportError(final String string) {
    	runOnUiThread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				mWifiScanCountView.setText(string);
			}
			
		});
	}


	public void onNextPositionButtonClicked(View v) throws InterruptedException
    {
    	int position = Integer.parseInt(mNextPositionIdEditView.getText().toString());
    	Logger.getInstance().logPathPosition(position);
    	Toast.makeText(getApplicationContext(), "Position "+position+" logged", Toast.LENGTH_SHORT).show();
    	mNextPositionIdEditView.setText(String.valueOf(position+1));
    }
    
    public void onPreviousPositionButtonClicked(View v) throws InterruptedException
    {
    	int position = Integer.parseInt(mNextPositionIdEditView.getText().toString());
    	Logger.getInstance().logPathPosition(position);
    	Toast.makeText(getApplicationContext(), "Position "+position+" logged", Toast.LENGTH_SHORT).show();
    	mNextPositionIdEditView.setText(String.valueOf(position-1));
    }
    
    public void onStopLoggingButtonClicked(final View v) throws InterruptedException
    {
    	AlertDialog.Builder alert = new AlertDialog.Builder(this);
    	alert.setTitle("Title");
    	alert.setMessage("Message");
    	// Set an EditText view to get user input 
    	final EditText input = new EditText(this);
    	alert.setView(input);
    	alert.setPositiveButton("Ok", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Logger.getInstance().logMessage(input.getText().toString());
				Logger.getInstance().stopLogging();
		    	Button button = (Button) v;
		    	button.setText("Logging stopped");
		    	finish();
		    	System.runFinalizersOnExit(true);
		        System.exit(0);
			}
    		
    	});

		alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener(){

			@Override
			public void onClick(DialogInterface dialog, int which) {
				Logger.getInstance().logMessage(input.getText().toString());				
			}
			
		});

    	alert.show();
    }
    
    private ServiceConnection conn = new ServiceConnection() {
        
        public void onServiceDisconnected(ComponentName name) {
        	mTrackingService.onDestroy();
        }
        
        public void onServiceConnected(ComponentName name, IBinder service) {
            MyBinder binder = (MyBinder)service;
            mTrackingService = binder.getService();
            mTrackingService.setElement(mStepCountView, mWalkingStateView, mHeadingView, mIPtext, mWifiView);
        }
    };
    
}

