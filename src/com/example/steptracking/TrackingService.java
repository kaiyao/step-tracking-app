package com.example.steptracking;

import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Vector;

import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.net.wifi.ScanResult;
import android.os.AsyncTask;
import android.os.Binder;
import android.os.IBinder;
import android.widget.EditText;
import android.widget.TextView;

public class TrackingService extends Service implements SensorEventListener{

	private TextView mStepCountView;
	private TextView mWalkingStateView;
	private TextView mAngleView;
	
	private SensorManager mSensorManager = null;
	BroadcastReceiver mWifiReceiver = null;
	IntentFilter mWifiIntent= null;
	
	private float[] mRotationMatrix = new float[16];
	private float[] mLinearVector = new float[4];
	private float[] mWorldAcce = new float[4];
	private float[] mInverseRotationMatrix = new float[16];
	private float[] mRotationVector = new float[4];
	//For testing
	private float[] mAcc = new float[4];
	private float[] mMag = new float[4];
	//Phone orientation
	private float[] orientVals = new float[3];
	private float angle = 0;
	private float mCurrentAngle;
	
	//Step Detection Thresholds and Variables
	double upperThresh = 0.5;
	double lowerThresh = -0.5;
	int maxDistThresh = 150;
	int minDistThresh = 15;
	int currentStepCount = 0;
	int minStepDistThresh = 15;
	int maxStepDistThresh = 150;
	int maxStillDistThresh = 600;
	float lastUpperPeak = -1;
	float lastLowerPeak = -1;
	long lastUpperPeakIndex = -1;
	long lastLowerPeakIndex = -1;
	long lastStepIndex = -1;
	long sampleCount = 0;
	int logcounter=0;
	
	int windowSize = 10;
	Queue<Float> window = new LinkedList<Float>();
	
	//Control Variables
	boolean isStepUpdate = false;
	boolean isRunning = true;
	boolean isWalking = false;
	
	//Vector to hold all angles calculated, each step may occupy multiple entries in the vector 
	Vector<headingTuple> accVector = new Vector<headingTuple>();
	int accVectorIndex = 0;
	
	
	long lastStepTime = 0;
	
	//Angle smoothing
	float azimuth = 0;
	float lastAzimuth = 361;
	float l = 0.5f;
	
	
	public void setElement(TextView t1, TextView t2, TextView t3, EditText e1, TextView t4)
	{
		mStepCountView = t1;
		mWalkingStateView = t2;
		mAngleView = t3;
		
		mStepCountView.setText("Step: "+ currentStepCount + "\n");
		mWalkingStateView.setText("Not Walking\n");
	}
	
	@Override
	public void onCreate() {
		 // get sensorManager and initialise sensor listeners
        mSensorManager = (SensorManager) this.getSystemService(SENSOR_SERVICE);
        initListeners();
        
        for(int i=0; i<4; i++)
    	{
    		mAcc[i] = 0;
    		mMag[i] = 0;
    		mRotationVector[i] = 0;
    	}

		try {		
			//Start the network task to send the data to the server for result visualisation
            //new StepTask().execute(null , null, null);
		    
		} catch (Exception e) {
			e.printStackTrace();
		} 
	}
	
	
	@Override
    public void onDestroy() {
    	super.onDestroy();
    	// unregister sensor listeners to prevent the activity from draining the device's battery.
    	mSensorManager.unregisterListener(this);
    	//this.unregisterReceiver(mWifiReceiver);
    	isRunning = false;
    }
	
	@Override
	public IBinder onBind(Intent intent) {
		IBinder result = null;
	    if (null == result) {
	        result = new MyBinder();
	    }
	    return result;
	}
	
	public class MyBinder extends Binder{
	    
	    public TrackingService getService(){
	        return TrackingService.this;
	    }
	}
	
    // This function registers sensor listeners for the accelerometer, magnetometer and gyroscope.
    public void initListeners(){

        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_ROTATION_VECTOR),
                SensorManager.SENSOR_DELAY_FASTEST);
        
        mSensorManager.registerListener(this,
                mSensorManager.getDefaultSensor(Sensor.TYPE_LINEAR_ACCELERATION),
                SensorManager.SENSOR_DELAY_FASTEST);

    }

	@Override
	public void onSensorChanged(SensorEvent event) {
		
		if (mAngleView == null) {
			return;
		}
		
		switch(event.sensor.getType()) {
		
		case Sensor.TYPE_ROTATION_VECTOR:
	    	//Calculate new rotation matrix
	    	SensorManager.getRotationMatrixFromVector(mRotationMatrix , event.values);	
	    	SensorManager.getOrientation(mRotationMatrix, orientVals);
	    	
	    	float azimuth = orientVals[0];
			float pitch = orientVals[1];
			float roll = orientVals[2];
			
			Logger.getInstance().logCompass(orientVals);
			mAngleView.setText(Math.round(Math.toDegrees(azimuth))+"");
			
	    	break;
	    	
	    case Sensor.TYPE_LINEAR_ACCELERATION:
	    	//Update rotation matrix, inverted version
	    	mLinearVector[0] = event.values[0];
	    	mLinearVector[1] = event.values[1];
	    	mLinearVector[2] = event.values[2];

	    	//SensorManager.getRotationMatrix(mRotationMatrix,null,mAcc,mMag);
	    	android.opengl.Matrix.invertM(mInverseRotationMatrix,0,mRotationMatrix,0);
	    	android.opengl.Matrix.multiplyMV(mWorldAcce,0,mInverseRotationMatrix,0,mLinearVector,0);
	    		  	    	
	    	//Update walking state and step count
	    	updateStep();
	    	break;	 
	    }
	}
	
	@Override
	public void onAccuracyChanged(Sensor sensor, int accuracy) {}

    public void onResetStepButtonClicked() throws InterruptedException
    {
    	//Stop the network task
    	isRunning = false;
    	
    	try {Thread.sleep(200);} catch(InterruptedException e) {}
    	
    	//Reset steps
    	currentStepCount = 0;
		lastLowerPeakIndex = -1;
		lastUpperPeakIndex = -1;
		lastStepIndex = -1;
		mStepCountView.setText("Step: "+ currentStepCount + "\n");
		//Restart the network task to inform the server the start of the new session
    	isRunning = true;
    	new Thread(new StepTask()).start();
    }
	
	public void updateStep()
	{
		//Increase current sample count
		sampleCount++;
		
		//If the user is standing still for too much time, reset the walking state
		if(sampleCount - lastStepIndex > maxStillDistThresh)
		{
			isWalking = false;
			mWalkingStateView.setText("Not Walking\n");
		}
		
		window.add(mWorldAcce[2]);
		
		//Detect steps based on zAcc 
		if(mWorldAcce[2]>upperThresh)
		{
			lastUpperPeak = mWorldAcce[2];
			lastUpperPeakIndex = sampleCount;
			
			if(lastLowerPeakIndex!=-1 && lastUpperPeakIndex-lastLowerPeakIndex<maxDistThresh 
				&& lastUpperPeakIndex-lastLowerPeakIndex>minDistThresh && sampleCount - lastStepIndex > minStepDistThresh)
			{
				//In the walking state, new step detected
				if(isWalking)
				{
					isStepUpdate = true;
					currentStepCount++;
					lastStepIndex = sampleCount;
					//Reset last lower peak for future steps
					lastLowerPeakIndex = -1;
					
					mWalkingStateView.setText("Walking\n");
					mStepCountView.setText("Step: "+ currentStepCount + "\n");
					
					Logger.getInstance().logStep();
				}else
				{
					//Not in the walking state, transit to the walking state if one candidate step detected
					if(sampleCount-lastStepIndex<maxStepDistThresh)
					{
						isWalking = true;
					}
					lastStepIndex = sampleCount;
				}
			}
		}else if(mWorldAcce[2]<lowerThresh)
		{
			if(mWorldAcce[2]<lastLowerPeak || sampleCount-lastLowerPeakIndex > maxDistThresh)
			{
				lastLowerPeak = mWorldAcce[2];
				lastLowerPeakIndex = sampleCount;
			}
		}	
	}	

	
	
	public float normalizeAngle(float angle){
		angle=(float) (angle%(2*Math.PI));
		return (float) (angle<0?angle+2*Math.PI:angle);
	}
	
	
    //Data structure to hold each heading input
	public class headingTuple
	{
		float horizentalAcc;
		float verticalAcc;
		float zAcc;
		int step;
		int angle;
		String direction;
		List<ScanResult> fingerPrint;
		
		headingTuple(float x, float y, float z, int a, String tag, int stepCount)
		{
			horizentalAcc = x;
			verticalAcc = y;
			zAcc = z;
			step = stepCount;
			angle = a;
			direction = tag;
			//fingerPrint = r;
		}	
	}
	
	//Task to send heading angle of each step to the server for visualisation 
	private class StepTask implements Runnable {

		@Override
		public void run() {
			try {
    		    while(isRunning)
    		    {
    		    	if(isStepUpdate)
    		    	{
    		    		//TODO: Do something when step updates
    		    		
    		    		
    		    		//Mark all the angles to 'used'
    		    		accVectorIndex = accVector.size();
    		    		isStepUpdate = false;
    		    	}
    		    }
            } catch (Exception e) {
                e.printStackTrace();
            } 
            return;			
		}    	
    }
}
